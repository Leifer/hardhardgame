//Définition des états des PV
//3 points de vie
if (global.pvJ == 3){
   sprite_index = global.sprIPV3
}

//2 points de vie
else if (global.pvJ == 2){
   sprite_index = global.sprIPV2
}

//1 point de vie
else if (global.pvJ == 1){
   sprite_index = global.sprIPV1
}

//Mort = relancer le jeu
else {
     game_restart();
}
