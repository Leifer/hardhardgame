VarGlobStatiques();
///SCRIPT STEP DE L'OBJET JOUEUR
//Déplacement Droite
if ((global.conDroite) && place_free(x+global.vitHor,y) && (!global.conGauche)){
   if (place_free(x,y+1)){
      if(place_free(x+global.vitHor,y+vspeed)){
         //Sprite Droite Saut/Chute
         sprite_index = global.sprJstatD;
            x+=global.vitHor;
      }
   }else{
      //Sprite Droite
      sprite_index = global.sprJdepD;
      image_speed = global.vitImg;
         x+=global.vitHor;
   }
//Déplacement Gauche
}else if ((global.conGauche) && place_free(x-global.vitHor,y) && (!global.conDroite)){
   if (place_free(x,y+1)){
      if (place_free(x-global.vitHor,y+vspeed)){
      //Sprite Gauche Saut/Chute
      sprite_index = global.sprJstatG;
            x-=global.vitHor;
            }
         }else{
      //Sprite Gauche
      sprite_index = global.sprJdepG;
      image_speed = global.vitImg;
         x-=global.vitHor;
         }
}

//Gravité
if (place_free(x,y+1)){
   gravity = global.vitGrav;
   //Vitesse max de chute
   if (vspeed >= global.vitVer){
      vspeed = global.vitVer;
   }
}else{
   gravity = 0;
   //Sauter
   if (global.conSaut){
      vspeed = -global.vitVer;
   }
}

//Sprite fixe quand immobile
if ((!global.conSaut) && (!global.conDroite) && (!global.conGauche) || (global.conDroite) && (global.conGauche)){
   if (place_free(x,y+1)){
      if(place_free(x,y+vspeed)){
      //Sprite Face Saut/Chute
      sprite_index = global.sprJstat;
            }
         }else{
      //Sprite Face
      sprite_index = global.sprJstat;
         }
}

//Relancer le niveau (A CHANGER)
//Avec le raccourcis ou contact avec un ennemi
if ((global.conRelancer)){
   game_restart();
}


//Quitter le jeu
if (global.conEchap){
   game_end();
}
