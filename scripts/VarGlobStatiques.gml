///VARIABLES GLOBALES STATIQUES
//Ces variables sont constantes durant l'excution du programme.

///VARIABLES CONTROLES
//Déplacement
global.conEchap = (keyboard_check(vk_escape));
global.conDroite = (keyboard_check(vk_right)) || (keyboard_check(ord('D')));
global.conGauche = (keyboard_check(vk_left)) || (keyboard_check(ord('Q')));
global.conSaut = (keyboard_check(vk_up)) || (keyboard_check(ord('Z'))) || (keyboard_check(vk_space));
global.conMenu = (keyboard_check(ord('M')));
global.conRelancer = (keyboard_check(ord('R')));


///VARIABLES DES SPRITES
//Sprites Joueur
global.sprJstat = spr_joueur_stat;
global.sprJdepD = spr_joueur_dep_D;
global.sprJdepG = spr_joueur_dep_G;
global.sprJstatD = spr_joueur_stat_D;
global.sprJstatG = spr_joueur_stat_G;

//Sprites Ennemis
//Robot
global.sprERodepD = spr_ennemi_robot_D;
global.sprERodepG = spr_ennemi_robot_G;

//Sprites Décors
//Bouton
global.sprDBinn = spr_bouton_innactif;
global.sprDBact = spr_bouton_actif;
global.sprDPfer = spr_porte_fermee;
global.sprDPouv = spr_porte_ouverte;

//Sprites Interface
global.sprIPV3 = spr_3pv;
global.sprIPV2 = spr_2pv;
global.sprIPV1 = spr_1pv;


///VARIABLES DE VITESSES
global.vitImg = .3;
global.vitHor = 2.4;
global.vitVer = 10;
global.vitGrav = .40;
global.vitEHor = 1;
