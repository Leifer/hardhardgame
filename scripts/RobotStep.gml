VarGlobStatiques();

//Déplacement à droite
if (dirERo == 0){
   x += global.vitEHor
   sprite_index = global.sprERodepD;
   image_speed = global.vitImg
}
//Déplacement à gauche
if (dirERo == 1){
   x -= global.vitEHor
   sprite_index = global.sprERodepG;
   image_speed = global.vitImg
}

//Changement de direction (vers la droite)
if (place_meeting(x-1,y,obj_bloc_collision) || place_empty(x-(sprite_width/2),y+1)){
   dirERo = 0;
}
//Changement de direction (vers la gauche)
if (place_meeting(x+1,y,obj_bloc_collision) || place_empty(x+(sprite_width/2),y+1)){
   dirERo = 1;
}
