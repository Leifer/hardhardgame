//Si le joueur à 3 Points de Vie
if (global.pvJ == 3) {
//Perd 1 point de vie
   global.pvJ = 2;
   //Déclenche l'alarme
   alarm[0] = room_speed/3;
   //Saute
   vspeed = -global.vitVer;
   //Deviens invulnérable   
   global.invul = 1;
   //Sprite change de couleur
   image_blend = c_red;
   
//Si le joueur à 2 Points de vie
} else if (global.pvJ == 2 && global.invul = 0) {
//Perd 1 point de vie
   global.pvJ = 1;
   //Déclenche l'alarme
   alarm[0] = room_speed/3;
   //Saute
   vspeed = -global.vitVer;
   //Deviens invulnérable
   global.invul = 1;
   //Sprite change de couleur
   image_blend = c_red;
   
//Si le joueur à 1 Point de vie
} else if (global.pvJ == 1 && global.invul = 0) {
   //Perd 1 point de vie
   global.pvJ = 0;
   //Déclenche l'alarme
   alarm[0] = room_speed/3;
   //Saute
   vspeed = -global.vitVer;
   //Deviens Invulnérable
   global.invul = 1;
   //Sprite change de couleur
   image_blend = c_red;
}
