VarGlobStatiques();

//Déplacement à droite
if (global.dirERo == 0){
   x += global.vitHor
   sprite_index = global.sprERodepD;
   image_speed = global.vitImg
}
//Déplacement à gauche
if (global.dirERo == 1){
   x -= global.vitHor
   sprite_index = global.sprERodepG;
   image_speed = global.vitImg
}

//Changement de direction (vers la droite)
if (place_meeting(x-1,y,obj_bloc_collision)){
   global.dirERo = 0;
}
//Changement de direction (vers la gauche)
if (place_meeting(x+1,y,obj_bloc_collision)){
   global.dirERo = 1;
}
